import LoginContainer from "../containers/LoginContainer";
import AccessDenied from "../components/AccessDenied";
import ProductListContainer from "../containers/ProductList";
import ViewCartContainer from "../containers/ViewCart";
import CheckoutContainer from "../containers/Checkout";
import AddUserContainer from "../containers/AddUser";
import ResourceNotFound from "../components/ResourceNotFound";

const routes = [
  { path: ["/","/ProductList"], exact: true, component:ProductListContainer},
  {path: "/viewCart", exact: true, component: ViewCartContainer},
  {path: "/checkout", exact: true, component: CheckoutContainer},
  {path: "/addUser", exact: true, component: AddUserContainer},
  { path: "/login", exact: true, component: LoginContainer },
  { path: "/someProtectedRoute", isProtected: true, component: AccessDenied },
  { path: "/resourcenotfound", component: ResourceNotFound },
  { component: ResourceNotFound }
];

export default routes;
