import React from "react";
const CacheContext = React.createContext({})

export const CacheProvider = CacheContext.Provider
export const CacheConsumer = CacheContext.Consumer
export default CacheContext;
