

import { SET_CART_DATA,SET_TOTAL_AMOUNT_FOR_CART } from "../../redux/actionTypes";

export default function ViewCart(
    state = {
        cartData: [],
        totalAmount: 0
    },
    action
) {
    switch (action.type) {
        case SET_CART_DATA:
            return Object.assign({}, state, {
                cartData: action.cartData
            });
            case SET_TOTAL_AMOUNT_FOR_CART:
                return Object.assign({}, state, {
                    totalAmount: action.totalAmount
                });

        default:
            return state;
    }
}