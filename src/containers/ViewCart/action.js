import apiService from "../../services/apiService";
import {SET_CART_DATA,SET_TOTAL_AMOUNT_FOR_CART} from "../../redux/actionTypes";
import { API_SUCESS_STATUS } from "../../App.config";

export function setCartData(cartData) {
    return {
      type: SET_CART_DATA,
      cartData
    };
  }

  export function setTotalAmount(totalAmount){
    return{
      type: SET_TOTAL_AMOUNT_FOR_CART,
      totalAmount
    };
  }
