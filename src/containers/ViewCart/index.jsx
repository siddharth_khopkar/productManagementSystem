import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import history from "../../history";
import ViewCartComponent from "../../components/ViewCartComponent";
import * as ViewCartDataAction from "./action";
import * as CheckoutAction from "../Checkout/actions";

class ViewCartContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            
        };
        this.redirectToHomePage =  this.redirectToHomePage.bind(this);
        this.clearCartData = this.clearCartData.bind(this);
        this.onCheckoutButtonClick = this.onCheckoutButtonClick.bind(this);

    }

    redirectToHomePage() {
        history.push("/");
    }

    clearCartData(){
        this.props.ViewCartDataAction.setCartData([]);
        this.props.ViewCartDataAction.setTotalAmount(0);
    }

    onCheckoutButtonClick(totalAmount){
        this.props.ViewCartDataAction.setTotalAmount(totalAmount);
        this.props.CheckoutAction.setIsUserOnCheckoutPage(true);
        history.push("/checkout");

    }

    render(){
        return(
            <>
            <ViewCartComponent 
            cartData = {this.props.cartData}
            redirectToHomePage = {this.redirectToHomePage}
            clearCartData = {this.clearCartData}
            onCheckoutButtonClick = {this.onCheckoutButtonClick}
            />
            </>
        );
    }
};

const mapStateToProps = state => {
    return {
        cartData: state.viewCart.cartData
    };
};

const mapDispatchToProps = dispatch => {
    return {
        ViewCartDataAction : bindActionCreators(ViewCartDataAction,dispatch),
        CheckoutAction: bindActionCreators(CheckoutAction,dispatch),
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(ViewCartContainer);
