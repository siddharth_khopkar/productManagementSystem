import {SET_USER_LIST} from "../../redux/actionTypes";

export function setUserList(userList){
    return{
      type: SET_USER_LIST,
      userList
    };
  }