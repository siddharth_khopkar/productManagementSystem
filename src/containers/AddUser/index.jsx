import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import history from "../../history";
import * as AddUserAction from "./actions";
import * as loginAction from "../LoginContainer/actions";
import {
    isEmpty, isValueValid,
    ALPHABET_PATTERN_WITH_SPECIAL_CHARACTERS,
    EMAIL_PATTERN,
    isLessThanMinLength,
    PASSWORD_PATTERN,
    isGreaterThanMaxLength,
    MOBILE_PATTERN,
    // ADDRESS_PATTERN,
    isValueMatch
} from "../../utilities/app";

import AddUserComponent from "../../components/AddUserComponent";

class AddUserContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            formData: {
                firstName: "",
                lastName: "",
                email: "",
                phoneNumber: "",
                password: "",
                confirmPassword: "",
                address: "",
                gender: ""

            },
            error: {
                firstName: "",
                lastName: "",
                email: "",
                phoneNumber: "",
                password: "",
                confirmPassword: "",
                address: "",
                gender: ""

            },
            isFormSubmitted: false,
            userExistMessage: ""
        };
        this.onhandleChange = this.onhandleChange.bind(this);
        this.onSubmitButtonClick = this.onSubmitButtonClick.bind(this);

    }

    componentDidMount() {

    };

    componentDidUpdate(prevProps) {
        if (prevProps.userData != this.props.userData) {
            this.props.isUserOnCheckoutPage ?
                history.push("/checkout")
                : history.push("/");

        }
    }

    onhandleChange(e) {
        let formDataObj = { ...this.state.formData };
        let name = e.target.name;
        let value = e.target.value;
        if (
            (name === "firstName" || name === "lastName") &&
            !isValueValid(value, ALPHABET_PATTERN_WITH_SPECIAL_CHARACTERS)
        ) {
            return;
        }

        formDataObj[name] = value;
        this.setState({ formData: formDataObj }, () => {
            this.validateData(name, value);
        });
    }

    validateData(name, value) {
        let errorObject = this.state.error;
        switch (name) {
            case "firstName":
                errorObject.firstName = isEmpty(value) ?
                    " First Name is Required"
                    : isLessThanMinLength(value, 2)
                        ? "First Name is too short"
                        : !isValueValid(value, ALPHABET_PATTERN_WITH_SPECIAL_CHARACTERS)
                            ? "Please enter valid First name"
                            : "";

                break;
            case "lastName":
                errorObject.lastName = isEmpty(value)
                    ? "Surname is Required"
                    : isLessThanMinLength(value, 2)
                        ? "Surname is too short"
                        : !isValueValid(value, ALPHABET_PATTERN_WITH_SPECIAL_CHARACTERS)
                            ? "Please enter valid Surname"
                            : "";
                break;
            case "email":
                errorObject.email = isEmpty(value)
                    ? "Email is Required"
                    : isGreaterThanMaxLength(value, 100)
                        ? "Email is Too Long"
                        : !isValueValid(value, EMAIL_PATTERN)
                            ? "Enter Valid Email"
                            : "";
                break;
            case "phoneNumber":
                errorObject.phoneNumber = isEmpty(value)
                    ? "Phone Number is Required"
                    : !isValueValid(value, MOBILE_PATTERN)
                        ? "Enter Valid Phone Number"
                        : "";
                break;
            case "password":
                errorObject.password = isEmpty(value)
                    ? "Password is Required"
                    : !isValueValid(value, PASSWORD_PATTERN)
                        ? "Enter Valid Password"
                        : "";
                break;
            case "confirmPassword":
                errorObject.confirmPassword = isEmpty(value)
                    ? "Password is Required"
                    : !isValueMatch(value, this.state.formData.password)
                        ? "Cornfirm Password should be same as Password"
                        : "";
                break;

            case "address":
                errorObject.address = isEmpty(value)
                    ? "Address is Required"
                    : isLessThanMinLength(value, 10)
                        ? "Address is too short"
                        : "";
                break;


            case "gender":
                errorObject.gender = isEmpty(value)
                    ? "Please Select Gender"
                    : "";
                break;
        }
        this.setState({ error: errorObject });
    }

    isAllFieldsValid() {

        let formErrors = this.state.error;
        let isAllValid = true;
        for (let fieldName in formErrors) {

            this.validateData(fieldName, this.state.formData[fieldName]);

            if (formErrors[fieldName].length > 0) {
                isAllValid = false;
            }
        }
        return isAllValid;
    }

    onSubmitButtonClick() {
        this.setState({ isFormSubmitted: true });
        if (this.isAllFieldsValid()) {
            let isUserEmailExists = this.props.userList.some(userList => userList['email'] === this.state.formData.email);
            if (!isUserEmailExists) {
                let userId = this.props.userList.length > 0 ? this.props.userData.length + 1 : 1;
                let userObject = {};
                userObject.id = userId;

                for (let fieldName in this.state.error) {
                    userObject[fieldName] = this.state.formData[fieldName];
                }
                delete userObject.confirmPassword;

                let updatedUserList = this.props.userList.splice();
                updatedUserList.push(userObject);
                this.props.AddUserAction.setUserList(updatedUserList);
                this.props.loginAction.setUserRegistered(userObject);

            }
            else {
                let message = "User Aleady Exists";
                this.setState({ userExistMessage: message });
            }

        }

    }


    render() {
        return (
            <>
                <AddUserComponent
                    addUserState={this.state}
                    onhandleChange={this.onhandleChange}
                    onSubmitButtonClick={this.onSubmitButtonClick}

                />
            </>
        );
    }
};

const mapStateToProps = state => {
    return {
        userList: state.addUser.userList,
        userData: state.login.userData,
        isUserOnCheckoutPage: state.checkout.isUserOnCheckoutPage
    };
};

const mapDispatchToProps = dispatch => {
    return {
        AddUserAction: bindActionCreators(AddUserAction, dispatch),
        loginAction: bindActionCreators(loginAction, dispatch)
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(AddUserContainer);

