

import { SET_USER_LIST } from "../../redux/actionTypes";

export default function AddUser(
    state = {
        userList: []
    },
    action
) {
    switch (action.type) {
        case SET_USER_LIST:
            return Object.assign({}, state, {
                userList: action.userList
            });
          

        default:
            return state;
    }
}