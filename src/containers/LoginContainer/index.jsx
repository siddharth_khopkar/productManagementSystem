import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Login from "../../components/Login";
import history from "../../history";
import * as loginAction from "./actions";
import { getValue } from "../../utilities/app";


import {
  isEmpty, isValueValid,
  ALPHABET_PATTERN_WITH_SPECIAL_CHARACTERS,
  EMAIL_PATTERN,
  isLessThanMinLength,
  PASSWORD_PATTERN,
  isGreaterThanMaxLength,
  MOBILE_PATTERN,
  ADDRESS_PATTERN,
  isValueMatch
} from "../../utilities/app";

class LoginContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formData: {
        email: "",
        password: ""
      },
      error: {
        email: "",
        password: ""
      },
      isFormSubmitted: false,
      errorMessage: ""
    };
    this.onhandleChange = this.onhandleChange.bind(this);
    this.redirectToAddUserPage = this.redirectToAddUserPage.bind(this);
    this.onSubmitButtonClick = this.onSubmitButtonClick.bind(this);

  }

  componentDidMount() {

  };


  componentDidUpdate(prevProps){
    if(prevProps.userData != this.props.userData){
        this.props.isUserOnCheckoutPage ? 
        history.push("/checkout")
        :  history.push("/");

    }
}


 
  onhandleChange(e) {
    let formDataObj = { ...this.state.formData };
    let name = e.target.name;
    let value = e.target.value;


    formDataObj[name] = value;
    this.setState({ formData: formDataObj }, () => {
      this.validateData(name, value);
    });
  }

  validateData(name, value) {
    let errorObject = this.state.error;
    switch (name) {
      case "email":
        errorObject.email = isEmpty(value)
          ? "Email is Required"
          : isGreaterThanMaxLength(value, 100)
            ? "Email is Too Long"
            : !isValueValid(value, EMAIL_PATTERN)
              ? "Enter Valid Email"
              : "";
        break;
      case "password":
        errorObject.password = isEmpty(value)
          ? "Password is Required"
          : !isValueValid(value, PASSWORD_PATTERN)
            ? "Enter Valid Password"
            : "";
        break;
    }
    this.setState({ error: errorObject });
  }

  isAllFieldsValid() {

    let formErrors = this.state.error;
    let isAllValid = true;
    for (let fieldName in formErrors) {

      this.validateData(fieldName, this.state.formData[fieldName]);

      if (formErrors[fieldName].length > 0) {
        isAllValid = false;
      }
    }
    return isAllValid;
  }

  onSubmitButtonClick() {
    this.setState({ isFormSubmitted: true });
    if (this.isAllFieldsValid()) {
      let userObject = this.props.userList.find(user => user['email'] === this.state.formData.email.trim() && user['password'] === this.state.formData.password.trim());
      if (userObject != undefined) {
        this.props.loginAction.setUserRegistered(userObject);
      }
      else {
        let message = "User does not exist";
        this.setState({ errorMessage: message });
      }
    }
  };

  redirectToAddUserPage(e) {
    e.preventDefault();
    history.push("/addUser");
  }
  render() {
    return (
      <>
        <Login
          loginState={this.state}
          redirectToAddUserPage={this.redirectToAddUserPage}
          onhandleChange={this.onhandleChange}
          onSubmitButtonClick={this.onSubmitButtonClick}
        />
      </>
    );
  };
};

const mapStateToProps = state => {
  return {
    userList: state.addUser.userList,
    isUserOnCheckoutPage: state.checkout.isUserOnCheckoutPage
  };
};

const mapDispatchToProps = dispatch => {
  return {
    loginAction: bindActionCreators(loginAction, dispatch)
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer);
