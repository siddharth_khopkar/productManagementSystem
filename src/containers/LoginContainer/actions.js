import {SET_REGISTERD_USER_DATA} from "../../redux/actionTypes";


export function setUserRegistered(userData) {
    return {
      type: SET_REGISTERD_USER_DATA,
      userData
    };
  }
  
