import {SET_REGISTERD_USER_DATA} from "../../redux/actionTypes";

export default function login(
    state = {
      userData: {}
    },
    action
  ) {
    switch (action.type) {
      case SET_REGISTERD_USER_DATA:
        return Object.assign({}, state, {
          userData: action.userData
        });
      default:
        return state;
    }
  }