

import { SET_IS_USER_ON_CHECKOUT_PAGE, SET_TRANSACTION_DATA } from "../../redux/actionTypes";

export default function Checkout(
    state = {
        isUserOnCheckoutPage: false,
        transactionData: []
    },
    action
) {
    switch (action.type) {
        case SET_IS_USER_ON_CHECKOUT_PAGE:
            return Object.assign({}, state, {
                isUserOnCheckoutPage: action.isUserOnCheckoutPage
            });
        case SET_TRANSACTION_DATA:
            return Object.assign({}, state, {
                transactionData: action.transactionData
            });


        default:
            return state;
    }
}