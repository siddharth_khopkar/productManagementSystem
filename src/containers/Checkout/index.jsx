import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import history from "../../history";
import * as CheckoutAction from "./actions";
import * as ViewCartDataAction from "../ViewCart/action";
import CheckoutComponent from "../../components/CheckoutComponent";
import ThankYouComponent from "../../components/ThankYouComponent";


class CheckoutContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            paymentMode: "cash",
            redirectToThankyouPage : false
        };
        this.onSubmitButtonClick = this.onSubmitButtonClick.bind(this);

    }

    componentDidMount() {
        if (Object.keys(this.props.userData).length <= 0) {
            history.push("/login");
        }
        else if (this.props.totalAmount <= 0) {
            history.push("/");
        }
    };

    componentDidUpdate(prevProps){
        if( this.props.transactionData != prevProps.transactionData){
            this.setState({redirectToThankyouPage: true});
        }
    }

    onSubmitButtonClick() {
        let transactionObject = {};
        console.log(this.props.transactionData);
        transactionObject.id = this.props.transactionData.length > 0 ? this.props.transactionData.length + 1 : 1;
        transactionObject.userId = this.props.userData.userId;
        let productIds = [];
        let randomNumber = "Trans"+Math.floor(100000 + Math.random() * 900000);

        Array.isArray(this.props.cartData) && this.props.cartData.length > 0 &&
            this.props.cartData.forEach((data) => {
                productIds.push(data.id);
            });

        transactionObject.productId = productIds;
        transactionObject.amount = this.props.totalAmount;
        transactionObject.paymentMode = this.state.paymentMode;
        transactionObject.transactionNumber = randomNumber;

        let addTransactionData = this.props.transactionData.splice();
        addTransactionData.push(transactionObject);
         this.props.CheckoutAction.setTransactionData(addTransactionData);
        this.clearCartData();



    };

    clearCartData() {
        this.props.ViewCartDataAction.setCartData([]);
        this.props.ViewCartDataAction.setTotalAmount(0);
    }




    render() {
        return (
            <>
            {!this.state.redirectToThankyouPage ?
                <CheckoutComponent
                    checkoutState={this.state}
                    totalAmount={this.props.totalAmount}
                    userData={this.props.userData}
                    onSubmitButtonClick={this.onSubmitButtonClick}
                /> :
                <ThankYouComponent
                userData={this.props.userData} 
                transactionData = {this.props.transactionData} 
                />
            }
            </>
        );
    }
};

const mapStateToProps = state => {
    return {

        userData: state.login.userData,
        totalAmount: state.viewCart.totalAmount,
        cartData: state.viewCart.cartData,
        transactionData: state.checkout.transactionData
    };
};

const mapDispatchToProps = dispatch => {
    return {
        CheckoutAction: bindActionCreators(CheckoutAction, dispatch),
        ViewCartDataAction: bindActionCreators(ViewCartDataAction, dispatch)
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(CheckoutContainer);

