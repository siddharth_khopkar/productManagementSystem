import {SET_IS_USER_ON_CHECKOUT_PAGE,SET_TRANSACTION_DATA} from "../../redux/actionTypes";

export function setIsUserOnCheckoutPage(isUserOnCheckoutPage){
    return{
      type: SET_IS_USER_ON_CHECKOUT_PAGE,
      isUserOnCheckoutPage
    };
  }


  export function setTransactionData(transactionData){
    return{
      type: SET_TRANSACTION_DATA,
      transactionData
    };
  }
