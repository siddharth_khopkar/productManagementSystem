

  import {GET_PRODUCTS_LISTS} from "../../redux/actionTypes";

export default function ProductList(
    state = {
        productData: []
    },
    action
  ) {
    switch (action.type) {
      case GET_PRODUCTS_LISTS:
        return Object.assign({}, state, {
            productData: action.productData
        });
       
      default:
        return state;
    }
  }