import apiService from "../../services/apiService";
import {GET_PRODUCTS_LISTS} from "../../redux/actionTypes";
import { API_SUCESS_STATUS } from "../../App.config";


export function setProductListData(productData) {
    return {
      type: GET_PRODUCTS_LISTS,
      productData
    };
  }
  
  export function getProductList() {
   
    return dispatch => {
      apiService.getProductList().then(
        response => {
             if(response.status == API_SUCESS_STATUS){
                 dispatch(setProductListData(response.data));
             }
        },
        error => {
       
        }
      );
    };
  }

  