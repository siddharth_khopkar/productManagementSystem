import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import history from "../../history";
import ProductListComponent from "../../components/ProductListComponent";
import * as ProductListAction from "./actions";
import * as ViewCartDataAction from "../ViewCart/action";

class ProductListContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedSortedBy: "",
            shouldShowSortedData: false,
            sortedByData: []
        };
        this.handleDropdownChange = this.handleDropdownChange.bind(this);
        this.redirectToViewCartPage = this.redirectToViewCartPage.bind(this);
        this.onAddToCartClick = this.onAddToCartClick.bind(this);
        this.onAnchorTagClick = this.onAnchorTagClick.bind(this);


    }

    componentDidMount() {
        this.props.ProductListAction.getProductList();
    }
    componentWillUnmount() {
        this.props.ProductListAction.setProductListData([]);
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.cartData != prevProps.cartData) {
            this.redirectToViewCartPage();
        }
    }


    handleDropdownChange(event) {
        let value = event.target.value;
        this.setState({ selectedSortedBy: event.target.value }, () => {
            this.getDataSortBy(value)
        });


    }

    getDataSortBy(value) {
        let sortedProductListData = this.props.productListData.slice();
        let shouldShowSortedDataFlag = false;
        switch (value) {
            case "name":
                sortedProductListData.sort(function (a, b) {
                    let nameA = a[value].toLowerCase(), nameB = b[value].toLowerCase();
                    if (nameA < nameB) return -1;
                    if (nameA > nameB) return 1;
                    return 0;
                });
                shouldShowSortedDataFlag = true;
                break;
            case "price":
                sortedProductListData.sort(function (a, b) {
                    return a[value] - b[value];
                });
                shouldShowSortedDataFlag = true;
                break;
        }
        this.setState({ sortedByData: sortedProductListData, shouldShowSortedData: shouldShowSortedDataFlag });
    }

    onAddToCartClick(cartData) {
        let cartDataToAdd = this.props.cartData.slice();
        cartDataToAdd.push(cartData);
        this.props.ViewCartDataAction.setCartData(cartDataToAdd);
    };


    redirectToViewCartPage() {
        history.push("/viewCart");
    }

    onAnchorTagClick(event){
        event.preventDefault();
        history.push("/login");
    }


    render() {
        return (
            <>
                <ProductListComponent
                    productListData={this.props.productListData}
                    productListStates={this.state}
                    handleDropdownChange={this.handleDropdownChange}
                    handleProductTitleClick={this.handleProductTitleClick}
                    redirectToViewCartPage={this.redirectToViewCartPage}
                    onAddToCartClick={this.onAddToCartClick}
                    onAnchorTagClick = {this.onAnchorTagClick}
                />
            </>
        );
    }
}

const mapStateToProps = state => {
    return {
        productListData: state.productList.productData,
        cartData: state.viewCart.cartData
    };
};

const mapDispatchToProps = dispatch => {
    return {
        ProductListAction: bindActionCreators(ProductListAction, dispatch),
        ViewCartDataAction: bindActionCreators(ViewCartDataAction, dispatch)
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(ProductListContainer);
