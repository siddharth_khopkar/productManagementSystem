import React, { Component } from "react";
import UserContext from './UserContext';

class UserProvider extends Component {
    
    render() {
        const user = JSON.parse(sessionStorage.getItem("user") || "{}");
        return (
            <UserContext.Provider
                value={user}
            >
                {this.props.children}
            </UserContext.Provider>
        );
    }
}
export default UserProvider;