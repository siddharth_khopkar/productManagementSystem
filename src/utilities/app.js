import history from "../history";
import _ from "lodash";




export const EMAIL_PATTERN =
  "^[A-Za-z0-9._%-+]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$";
export const PASSWORD_PATTERN =
  "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~])[A-Za-z\\d!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~]{8,20}$";
export const ALPHABET_PATTERN_WITH_SPECIAL_CHARACTERS = "^[a-zA-Z '-]*$";
export const NUMBER_PATTERN = "^[0-9]*$";
export const COUNTRY_CODE_PATTERN = "[\\+][0-9]{1,5}";
export const MOBILE_PATTERN = /^(\+\d{1,3}[- ]?)?\d{10}$/;
export const ONLY_NUMBER_PATTERN = /^\d+$/;
export const ADDRESS_PATTERN = "^[#.0-9a-zA-Z\s,-]+$";



export const getValue = (obj, expression) => {
  try {
    return expression.split(".").reduce((o, x, i) => {
      if (i === 0) {
        return o;
      }
      return typeof o === "undefined" || o === null ? undefined : o[x];
    }, obj);
  } catch (e) {
    console.error("getValue => " + e);
    return undefined;
  }
};

export const isEmpty = value => {
  return (
    !value ||
    value === null ||
    (typeof value === "string" && value.trim() === "")
  );
};

export const isNumeric = value => {
  if (!value) return true;
  const reg = new RegExp("^[0-9]+$");
  return reg.test(value);
};

export const isValueValid = (value, regExp) => {
  const reg = new RegExp(regExp);
  return reg.test(value);
};

export const isValueMatch = (value, matchValue) => {
  return value == matchValue;
};
export const isLessThanMinLength = (value, length) => {
  return value.length <= length;
};

export const isGreaterThanMaxLength = (value, length) => {
  return value.length >= length;
};


export const SORT_BY = {
  "" : "Select Sort By",
  "name" : "Name",
  "price" : "Price"
};

export const Table_HEADER_FOR_VIEW_CART = {
"id": " id",
"name" : "Product Name",
"price" : "Price"
};



