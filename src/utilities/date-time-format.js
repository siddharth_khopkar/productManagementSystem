import moment from "moment";
const monthNames = [
  "Jan",
  "Feb",
  "March",
  "April",
  "May",
  "June",
  "July",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec"
];

export function checkIfCurrentDateIsBetweenStartDateAndEndDate(
  startDate,
  endDate
) {
  const startDt = new Date(startDate);
  const endDt = new Date(endDate);
  const currentDate = new Date();

  if (currentDate > startDt && currentDate < endDt) {
    return true;
  }
  return false;
}

export function getCurrentDate() {
  return moment();
}

export const getDateDiffForNotification = (date, currentDate) => {
  let serverDate = moment(date);
  let duration = moment.duration(currentDate.diff(serverDate));

  let returnStr = "";
  let diffData = duration._data;
  let AMPM = diffData.hours <= 12 ? "AM" : "PM";
  let serverDateWithoutTime = moment({
    year: serverDate.year(),
    month: serverDate.month(),
    day: serverDate.date()
  });
  let currentDateWithoutTime = moment({
    year: currentDate.year(),
    month: currentDate.month(),
    day: currentDate.date()
  });
  const daysDiff = currentDateWithoutTime.diff(serverDateWithoutTime, "days");
  if (daysDiff >= 2) {
    AMPM = getAMPM(serverDate.hours());
    returnStr =
      returnStr +
      // formatDateForMyCrowd(getDateBasedOnLocation(apiDate)) +
      formatDateForMyCrowd(serverDate) +
      " " +
      serverDate.format("h") +
      ":" +
      serverDate.minutes() +
      " " +
      AMPM;
  } else if (daysDiff === 1) {
    AMPM = getAMPM(serverDate.hours());
    returnStr =
      "Yesterday " +
      serverDate.format("h") +
      ":" +
      serverDate.minutes() +
      " " +
      AMPM;
  } else if (diffData.hours > 12) {
    returnStr =
      "Today " + (diffData.hours % 12) ||
      12 + ":" + diffData.minutes + " " + AMPM;
  } else if (diffData.hours >= 1 && diffData.hours <= 12) {
    returnStr = diffData.hours + " hours ago";
  } else if (diffData.hours < 1 && diffData.minutes > 1) {
    returnStr = diffData.minutes + " minutes ago";
  } else if (diffData.minutes <= 1) {
    returnStr = "Just Now";
  }
  return returnStr;
};
export function renderModifiedDate(modifiedDate) {
  let date = "";
  if (getDateBasedOnLocation(modifiedDate).isValid()) {
    if (
      getDifferenceIWithCurrentDate(
        "hour",
        getDateBasedOnLocation(modifiedDate)
      ) >= 24
    ) {
      date = date + formatDateForMyCrowd(getDateBasedOnLocation(modifiedDate));
    }
  }
  return date;
}
export function renderModifiedTime(modifiedDate) {
  let time = "";
  if (getDateBasedOnLocation(modifiedDate).isValid()) {
    time = formatTimeForSchedulePage(getDateBasedOnLocation(modifiedDate));
  }
  return time;
}
export function getDifferenceIWithCurrentDate(value, startDate, endDate) {
  let apiDate = startDate;
  let currentDate;
  if (endDate) {
    currentDate = endDate;
  } else {
    currentDate = new Date();
  }
  var start_date = moment(apiDate, "YYYY-MM-DD HH:mm:ss");
  var end_date = moment(currentDate, "YYYY-MM-DD HH:mm:ss");
  var duration = moment.duration(end_date.diff(start_date));
  if (value === "hour") {
    return duration.asHours();
  } else if (value === "mins") {
    return duration.asMinutes();
  }
}

export function formatTimeForSchedulePage(startDateString, endDateString) {
  let startDate;
  let endDate;

  if (startDateString) {
    startDate = new Date(startDateString);
  }
  if (endDateString) {
    endDate = new Date(endDateString);
  }
  if (isDateType(startDate) && isDateType(endDate)) {
    return (
      getHours(startDate) +
      ":" +
      getMinutes(startDate) +
      "-" +
      getHours(endDate) +
      ":" +
      getMinutes(endDate) +
      " " +
      getAMPM(endDate.getHours())
    );
  } else if (isDateType(startDate)) {
    return (
      getHours(startDate) +
      ":" +
      getMinutes(startDate) +
      " " +
      getAMPM(startDate.getHours())
    );
  } else {
    return "";
  }
}

export function formatDateForSchedulePage(date) {
  let dateObj = new Date(date);
  return (
    getDate(dateObj) +
    " " +
    monthNames[dateObj.getMonth()] +
    " " +
    dateObj.getFullYear()
  );
}

export function formatDateForMyCrowd(date) {
  let dateObj = new Date(date);
  return (
    getDate(dateObj) +
    " " +
    monthNames[dateObj.getMonth()] +
    " '" +
    dateObj
      .getFullYear()
      .toString()
      .substr(-2)
  );
}

function getAMPM(hours) {
  return hours >= 12 ? "PM" : "AM";
}
function getHours(date) {
  return date.getHours() % 12 === 0 ? "12" : date.getHours() % 12;
}
function getMinutes(date) {
  return date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
}

function getDate(date) {
  return date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
}
export function commentTime(date) {
  let commentDate = new Date(date);
  //   08/12/2019 . 05:00 pm
  return (
    getHours(commentDate) +
    ":" +
    getMinutes(commentDate) +
    " " +
    getAMPM(commentDate.getHours())
  );
}

function isDateType(date) {
  return date instanceof Date;
}
export function submittedDate(date) {
  let submitDate = new Date(date);
  //   08/12/2019 . 05:00 pm
  return (
    getDate(submitDate) +
    "/" +
    (parseInt(submitDate.getMonth()) + 1) +
    "/" +
    submitDate.getFullYear() +
    "  " +
    getHours(submitDate) +
    ":" +
    getMinutes(submitDate) +
    " " +
    getAMPM(submitDate.getHours())
  );
}

export function commentDate(date) {
  let commentDate = new Date(date);
  //   08/12/2019 . 05:00 pm
  return (
    getDate(commentDate) +
    " " +
    monthNames[commentDate.getMonth()] +
    " '" +
    commentDate
      .getFullYear()
      .toString()
      .substr(-2)
  );
}

export function getDateBasedOnLocation(date) {
  let timeZone = new Date().getTimezoneOffset();
  return moment(date).utcOffset(timeZone);
}
