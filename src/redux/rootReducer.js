import { combineReducers } from "redux";
import loginReducer from "../containers/LoginContainer/reducers";
import productListReducer from "../containers/ProductList/reducers";
import ViewCartReducer from "../containers/ViewCart/reducers";
import AddUserReducer from "../containers/AddUser/reducers"
import CheckoutReducer from "../containers/Checkout/reducers";

export default combineReducers({
    login: loginReducer,
    productList: productListReducer,
    viewCart: ViewCartReducer,
    checkout: CheckoutReducer,
    addUser: AddUserReducer
});
