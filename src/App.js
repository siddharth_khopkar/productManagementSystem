import React from "react";
import { Provider } from "react-redux";

import routes from "./router/routes";
import ApplicationRouter from "./router/ApplicationRouter";
import configureStore from "./redux/store";
import "./styles.css";

export default function App() {
  return (
    <Provider store={configureStore()}>
      <div className="App">
        <ApplicationRouter routes={routes} />
      </div>
    </Provider>
  );
}
