import React from "react";

const AccessDenied = props => {
  return (
    <>
      <h2>Access Denied | Please login to access this resource...</h2>
    </>
  );
};

export default AccessDenied;
