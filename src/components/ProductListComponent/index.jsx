import React from "react";

import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';

import { SORT_BY } from "../../utilities/app";


import GridViewComponent from "../GridViewComponent";

const ProductListComponent = props => {
    const useStyles = makeStyles((theme) => ({
        root: {
            flexGrow: 1,
        },

        formControl: {
            margin: theme.spacing(1),
            minWidth: 120,
        },
        selectEmpty: {
            marginTop: theme.spacing(2),
        }
    }));


    return (
        <div className={useStyles().root}>
            <Grid container spacing={3}>
                <Grid item xs={6}> </Grid>
                <Grid item xs={6}>
                <Typography className={useStyles().root}>
      <Link href="#" onClick={(e)=> {props.onAnchorTagClick(e)}}>
        Login / Register
      </Link>
      </Typography>
                </Grid>
            </Grid>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <h1>Product List Page</h1>
                </Grid>
            </Grid>
            <Grid container spacing={3}>
                <Grid item xs={6}> </Grid>
                <Grid item xs={6}>
                    <Button variant="contained" color="secondary" onClick={props.redirectToViewCartPage}>
                        View Cart
      </Button>
                </Grid>
            </Grid>
            <Grid container spacing={3}>
                <Grid item xs={6}>
                    <h3>Product Sort By: </h3>
                </Grid>
                <Grid item xs={6}>
                    <FormControl className={useStyles().formControl}>
                        <Select
                            value={props.productListStates.selectedSortedBy}
                            onChange={(e) => { props.handleDropdownChange(e) }}
                            displayEmpty
                            className={useStyles().selectEmpty}
                            inputProps={{ 'aria-label': 'Without label' }}
                        >
                            {
                                Object.keys(SORT_BY).length > 0 && Object.keys(SORT_BY).map((key) =>
                                    <MenuItem value={key}>{SORT_BY[key]}</MenuItem>
                                )
                            }
                        </Select>
                    </FormControl>
                </Grid>
            </Grid>

            <Grid container spacing={3}>
                {
                    Array.isArray(props.productListData) && props.productListData.length > 0 ?
                        (props.productListStates.shouldShowSortedData ?

                            props.productListStates.sortedByData.map((sortedByData, index) => {
                                return (
                                    <GridViewComponent
                                        handleProductTitleClick={props.handleProductTitleClick}
                                        productData={sortedByData}
                                        onAddToCartClick={props.onAddToCartClick}
                                    />
                                );
                            })
                            :
                            props.productListData.map((prodData, index) => {
                                return (
                                    <GridViewComponent
                                        handleProductTitleClick={props.handleProductTitleClick}
                                        productData={prodData}
                                        onAddToCartClick={props.onAddToCartClick}
                                    />
                                );
                            })
                        ) :
                        <Grid item xs={12}>
                            <h3>No Data Found</h3>
                        </Grid>

                }
            </Grid>


        </div>
    );

}

export default ProductListComponent;