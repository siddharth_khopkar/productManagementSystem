import React from "react";

import { FormControl } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

import FormControlLabel from '@material-ui/core/FormControlLabel';

import Button from '@material-ui/core/Button';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';


const AddUserComponent = props => {
    const useStyles = makeStyles((theme) => ({
        root: {
            '& .MuiTextField-root': {
                margin: theme.spacing(1),
                width: 200,
            },
            flexGrow: 1,
        },
        container: {
            display: 'flex',
            flexWrap: 'wrap',
        },
        textField: {
            marginLeft: theme.spacing(1),
            marginRight: theme.spacing(1),
            width: 200,
        },
    }));

    return (
        <>

            <FormControl>
                <h1>Add Users</h1>
                <div className={useStyles.root}>
                    <Grid container spacing={3}>
                        <Grid item xs={6}>
                            <span> First Name : </span>
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                type="text"
                                id="outlined-basic"
                                name="firstName"
                            value={props.addUserState.formData.firstName}
                            onChange={(e) => {
                                props.onhandleChange(e)
                            }}
                            />
                        </Grid>
                        <Grid item xs={6}>
                        </Grid>
                        <Grid item xs={6}>
                            {props.addUserState.isFormSubmitted &&
                                <span className="error"> {props.addUserState.error.firstName} </span>
                            }
                        </Grid>

                    </Grid>

                    <Grid container spacing={3}>
                        <Grid item xs={6}>
                            <span>Last Name : </span>
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                type="text"
                                id="outlined-basic"
                                name="lastName"
                            value={props.addUserState.formData.lastName}
                            onChange={(e) => {
                                props.onhandleChange(e)
                            }}
                            />
                        </Grid>
                        <Grid item xs={6}>
                        </Grid>
                        <Grid item xs={6}>
                            {props.addUserState.isFormSubmitted &&
                                <span className="error"> {props.addUserState.error.lastName}</span>
                            }
                        </Grid>

                    </Grid>

                    <Grid container spacing={3}>
                        <Grid item xs={6}>
                            <span> Email : </span>
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                id="outlined-basic"
                                name="email"
                            value={props.addUserState.formData.email}
                            onChange={(e) => {
                                props.onhandleChange(e)
                            }}
                            />
                        </Grid>
                        <Grid item xs={6}>
                        </Grid>
                        <Grid item xs={6}>
                            {props.addUserState.isFormSubmitted &&
                                <span className="error"> {props.addUserState.error.email} </span>
                            }
                        </Grid>

                    </Grid>



                    <Grid container spacing={3}>
                        <Grid item xs={6}>
                            <span> Phone Number : </span>
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                id="outlined-basic"
                                name="phoneNumber"
                               
                            value={props.addUserState.formData.phoneNumber}
                            onChange={(e) => {
                                props.onhandleChange(e)
                            }}
                            />
                        </Grid>
                        <Grid item xs={6}>
                        </Grid>
                        <Grid item xs={6}>
                            {props.addUserState.isFormSubmitted &&
                                <span className="error"> {props.addUserState.error.phoneNumber} </span>
                            }
                        </Grid>

                    </Grid>


                    <Grid container spacing={3}>
                        <Grid item xs={6}>
                            <span> Password : </span>
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                id="outlined-basic"
                                name="password"
                            value={props.addUserState.formData.password}
                            placeholder= "Abcd@123"
                            type="password"
                            onChange={(e) => {
                                props.onhandleChange(e)
                            }}
                            />
                        </Grid>
                        <Grid item xs={6}>
                        </Grid>
                        <Grid item xs={6}>
                            {props.addUserState.isFormSubmitted &&
                                <span className="error"> {props.addUserState.error.password} </span>
                            }
                        </Grid>

                    </Grid>


                    <Grid container spacing={3}>
                        <Grid item xs={6}>
                            <span> Confirm Password : </span>
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                id="outlined-basic"
                                name="confirmPassword"
                                type="password"
                            value={props.addUserState.formData.confirmPassword}
                            onChange={(e) => {
                                props.onhandleChange(e)
                            }}
                            />
                        </Grid>
                        <Grid item xs={6}>
                        </Grid>
                        <Grid item xs={6}>
                            {props.addUserState.isFormSubmitted &&
                                <span className="error"> {props.addUserState.error.confirmPassword} </span>
                            }
                        </Grid>

                    </Grid>

                    <Grid container spacing={3}>
                        <Grid item xs={6}>
                            <span> Address: </span>
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                id="standard-multiline-static"
                                name="address"
                                placeholder = "1, Prahlad nagar, Ahemdabad - 11 "
                                multiline
                                rows={4}
                            value={props.addUserState.formData.address}
                            onChange={(e) => {
                                props.onhandleChange(e)
                            }}
                            />
                        </Grid>
                        <Grid item xs={6}>
                        </Grid>
                        <Grid item xs={6}>
                            {props.addUserState.isFormSubmitted &&
                                <span className="error"> {props.addUserState.error.address} </span>
                            }
                        </Grid>

                    </Grid>

                    <Grid container spacing={3}>
                        <Grid item xs={6}>
                            <span>Gender: </span>
                        </Grid>
                        <Grid item xs={6}>
                            <RadioGroup aria-label="gender" name="gender"
                            value={props.addUserState.formData.gender}
                            onChange={(e) => {
                                props.onhandleChange(e)
                            }}
                            >
                                <FormControlLabel value="female" control={<Radio />} label="Female" />
                                <FormControlLabel value="male" control={<Radio />} label="Male" />
                                <FormControlLabel value="other" control={<Radio />} label="Other" />
                            </RadioGroup>
                        </Grid>
                        <Grid item xs={6}>
                        </Grid>
                        <Grid item xs={6}>
                            { props.addUserState.isFormSubmitted && 
                            <span className="error">  {props.addUserState.error.gender} </span>
                        }
                        </Grid>

                    </Grid>

                    <Grid container spacing={3}>
                        <Grid item xs={12}>
                        <span className="error"> {props.addUserState.userExistMessage} </span>
                            </Grid>
                            </Grid>

                    <Grid container spacing={3}>
                        <Grid item xs={6}>
                        </Grid>
                        <Grid item xs={6}>
                            <Button variant="contained" color="primary"
                            onClick={
                                props.onSubmitButtonClick
                            }
                            >
                                Save
                                        </Button>
                        </Grid>
                    </Grid>

                </div>



            </FormControl>

        </>
    );
};

export default AddUserComponent;