import React, { useState } from "react";
import "./style.css";

const Login = props => {
  

  return (
    <div className="container text-center login-form">
     
        <div className="form-group">
          <h1>Login </h1>
        </div>
        <div className="form-group">
          <label for="exampleInputEmail1">Email address</label>
          <input
            type="email"
            value={props.loginState.formData.email}
            className="form-control"
            name="email"
            aria-describedby="emailHelp"
            placeholder="Enter email"
            onChange={e => {
              props.onhandleChange(e)
            }}
          />
            {props.loginState.isFormSubmitted && (
            <small className="form-text error">{props.loginState.error.email}</small>
          )}
        </div>
        <div className="form-group">
          <label for="exampleInputPassword1">Password</label>
          <input
            type="password"
            value={props.loginState.formData.password}
            onChange={e => {
              props.onhandleChange(e)
            }}
            className="form-control"
            name="password"
            placeholder="Password"
          />
          {props.loginState.isFormSubmitted  && (
            <small className="form-text error">{props.loginState.error.password}</small>
          )}
        </div>
        <div className="form-group">
          <a onClick= {(e) => props.redirectToAddUserPage(e)}> Register Here </a>
        </div>
        <div className="form-group">
        <small className="form-text error">{props.loginState.errorMessage}</small>
        </div>
        <button type="submit" className="btn btn-primary" onClick ={props.onSubmitButtonClick}>
          Login
        </button>
       
      
      
   
    </div>
  );
};

export default Login;
