import React from "react";

import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';

const ViewGridDataComponent = props => {
    const useStyles = makeStyles((theme) => ({
        paper: {
            padding: theme.spacing(2),
            textAlign: 'center',
            color: theme.palette.text.secondary,
        },

    }));
    return (
        <>
            <Grid item xs={12} sm={6}>
                <Paper className={useStyles.paper}>
                    Name :
                        {props.productData.name}
                </Paper>


                <Paper className={useStyles.paper}>Price: ${props.productData.price}</Paper>
                <Paper className={useStyles.paper}>Description: <p> {props.productData.description} </p></Paper>
                <Paper className={useStyles.paper}> 
                <Button variant="contained" color="primary" onClick={() => {props.onAddToCartClick(props.productData)}}>
                    Add to Cart
      </Button> </Paper>
                <br />
            </Grid>
        </>

    );

};

export default ViewGridDataComponent;