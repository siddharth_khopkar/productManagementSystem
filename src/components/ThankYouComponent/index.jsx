import React from "react";

import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import DoneOutlineIcon from '@material-ui/icons/DoneOutline';

const ThankYouComponent = props => {

    const useStyles = makeStyles((theme) => ({
        root: {
            flexGrow: 1,
        },
        
        formControl: {
            margin: theme.spacing(1),
            minWidth: 120,
        },
        selectEmpty: {
            marginTop: theme.spacing(2),
        }
    }));

    function getTransactionDetails(){
        let userDetails = props.transactionData.find( transactionData => transactionData['userId'] === props.userData.userId);
        return userDetails.transactionNumber;
    }

return(
  
<div className={useStyles().root}>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <DoneOutlineIcon />
                </Grid>
                </Grid>

                <Grid container spacing={3}>
                <Grid item xs={12}>
                    <h1>Sucessfully Order Completed</h1>
                </Grid>
                </Grid>
                
                <Grid container spacing={3}>
                <Grid item xs={12}>
                    <p> Thank you {props.userData.firstName + "  " + props.userData.lastName} , the transaction Number for your Product is <b>{getTransactionDetails()} </b>. 
                    Your Product will be delivered soon in next 3 - 4 working days.
                    </p>
                </Grid>
                </Grid>
          

        </div>

);
};

export default ThankYouComponent;