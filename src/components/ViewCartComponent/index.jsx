import React from "react";
import { Table_HEADER_FOR_VIEW_CART } from "../../utilities/app"

import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

const ViewCartComponent = props => {
    const useStyles = makeStyles({
        table: {
            minWidth: 650,
        },
    });

    function setDefaultTableData() {
        let headerLength = Object.keys(Table_HEADER_FOR_VIEW_CART).length > 0 ? Object.keys(Table_HEADER_FOR_VIEW_CART).length : 1;
        return (
            <>
                <th colspan={headerLength} align="center">No Records found</th>
            </>
        );
    }

    function calculateTotalAmount() {
        let totalAmount = 0;

        Array.isArray(props.cartData) && props.cartData.length > 0 &&
            props.cartData.map((data) => {
                totalAmount += data.price
            });

        return totalAmount;
    }

    return (
        <>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <h1> View Cart Detail</h1>
                </Grid>
            </Grid>
            <Grid container spacing={3}>
                <Grid item xs={6}>
                    <Button variant="contained" color="primary" onClick={props.redirectToHomePage}>
                        Back
      </Button>
                </Grid>
                <Grid item xs={6}>
                    {Array.isArray(props.cartData) && props.cartData.length > 0 &&
                        <Button variant="contained" color="secondary" onClick={props.clearCartData}>
                            Clear Cart
      </Button>
                    }
                </Grid>
            </Grid>
            <br />  <br />
            <TableContainer component={Paper}>
                <Table className={useStyles().table} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            {
                                Object.keys(Table_HEADER_FOR_VIEW_CART).length > 0 && Object.keys(Table_HEADER_FOR_VIEW_CART).map((key) =>
                                    <TableCell>{Table_HEADER_FOR_VIEW_CART[key]}</TableCell>
                                )
                            }
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            Array.isArray(props.cartData) && props.cartData.length > 0 ?
                                props.cartData.map((data, index) => {
                                    return (
                                        <TableRow>
                                            <TableCell> {index + 1} </TableCell>
                                            <TableCell> {data.name} </TableCell>
                                            <TableCell> ${data.price} </TableCell>
                                        </TableRow>
                                    )
                                })

                                :
                                <TableRow>
                                    <TableCell>{setDefaultTableData()}</TableCell>
                                </TableRow>
                        }
                    </TableBody>
                </Table>
            </TableContainer>
            <br />  <br />
            <Grid container spacing={3}>

                <Grid item xs={6}>
                    <span> Total :</span>
                </Grid>
                <Grid item xs={6}>
                    <span>${calculateTotalAmount()}</span>
                </Grid>

            </Grid>

            <Grid container spacing={3}>
                <Grid item xs={12}>
                {Array.isArray(props.cartData) && props.cartData.length > 0 &&
                        <Button variant="contained" color="primary" onClick={()=>{props.onCheckoutButtonClick(calculateTotalAmount())}}>
                           Checkout 
      </Button>
                    }
                </Grid>
            </Grid>


        </>
    );

};

export default ViewCartComponent;