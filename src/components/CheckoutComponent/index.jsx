import React from "react";

import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';


const CheckoutComponent = props => {

    const useStyles = makeStyles((theme) => ({
        root: {
            '& .MuiTextField-root': {
                margin: theme.spacing(1),
                width: 200,
            },
            flexGrow: 1,
        },
        container: {
            display: 'flex',
            flexWrap: 'wrap',
        },
        textField: {
            marginLeft: theme.spacing(1),
            marginRight: theme.spacing(1),
            width: 200,
        },
    }));

    return (
        <>
            <h1> Checkout Page </h1>
            <div className={useStyles.root}>
                <br />  <br />
                <Grid container spacing={3}>
                    <Grid item xs={6}>
                        <b> Amount : </b>
                    </Grid>
                    <Grid item xs={6}>
                        <span> ${props.totalAmount} </span>
                    </Grid>
                </Grid>
                <Grid container spacing={3}>
                    <Grid item xs={6}>
                        <b> Payment Mode : </b>
                    </Grid>
                    <Grid item xs={6}>

                        <RadioGroup aria-label="gender" name="gender"
                            value={props.checkoutState.paymentMode}

                        >
                            <FormControlLabel value="cash" control={<Radio />} label="Cash" />

                        </RadioGroup>

                    </Grid>
                </Grid>
                <Grid container spacing={3}>
                    <Grid item xs={6}>
                        <b> User Name: </b>
                    </Grid>
                    <Grid item xs={6}>
                        {Object.keys(props.userData).length > 0 &&
                            <span> {props.userData.firstName + "  " + props.userData.lastName}</span>
                        }
                    </Grid>
                </Grid>

                <Grid container spacing={3}>
                    <Grid item xs={6}>
                        <b> Phone Number: </b>
                    </Grid>
                    <Grid item xs={6}>
                        {Object.keys(props.userData).length > 0 &&
                            <span> {props.userData.phoneNumber}</span>
                        }
                    </Grid>
                </Grid>

                <Grid container spacing={3}>
                    <Grid item xs={6}>
                        <b> Address: </b>
                    </Grid>
                    <Grid item xs={6}>
                        {Object.keys(props.userData).length > 0 &&
                            <p> {props.userData.address}</p>
                        }
                    </Grid>
                </Grid>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <Button variant="contained" color="primary"
                            onClick={
                                props.onSubmitButtonClick
                            }
                        >
                            Place Order
                                        </Button>
                    </Grid>
                </Grid>
            </div>
        </>
    );

};

export default CheckoutComponent;