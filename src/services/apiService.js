import { request } from "../utilities/request";
import {  } from "../utilities/app";
import { API_PREFIX } from "../App.config";
// import * as signInAction from "../containers/Login/actions";
import { getLocalStorageAsJSON, deleteSessionStorage } from "../storage";
import history from "../history";
import axios from "axios";
import { getValue } from "../utilities/app";
import { getSessionStorage } from "../storage";
const CancelToken = axios.CancelToken;
let source;

export function logout() {
  deleteSessionStorage("user");
  history.push("/login");
}
export function getEvent(eventId) {

  let url = window.location.origin;
  return request(
   url,
    "GET",
    {},
    ""
  )
    .then(response => {
      return response;
    })
    .catch(error => {
      console.log(error);
      //TODO: when notification component will come we have to send notification
    });
}

export function getData(data) {
  // const url =
  //   getLocalStorageAsJSON("configurations")["IHUB_SERVICE_API_HOST"] +
  //   "/user-management" +
  //   API_VERSION +
  //   "/profile/user-details";
    const url = window.location.origin + "/stubs/forgotPassword-getUserData.json";
  return request(url, "GET", {}, data, false)
    .then(response => {
      return response;
    })
    .catch(error => {
      throw error;
    });
}

export function postData(
  data,
  pageIndex,
  pageSize,
  filterSortValue,
  filterSortOnValue
) {
  if (source) {
    source.cancel("Cancelling request");
  }
  source = CancelToken.source();
  let url = window.location.origin;
  if (pageIndex) {
    url += "&page=" + pageIndex;
  }
  
  return request(url, "POST", {}, data, true, { cancelToken: source.token })
    .then(response => {
      return response;
    })
    .catch(error => {
      console.log(error);
      throw error;
      //TODO: when notification component will come we have to send notification
    });
}


export function deleteData(body) {
  const url =
  window.location.origin;
  return request(url, "PATCH", {}, body)
    .then(response => {
      return response;
    })
    .catch(error => {
      throw error;
    });
}

export function patchData(
  collaboratorId,
  collaboratorStatus
) {
  const url = window.location.origin;

  return request(url, "PATCH")
    .then(response => {
      return response;
    })
    .catch(error => {
      throw error;
    });
}

export function getProductList() {
  let data = {};
    const url = API_PREFIX + "/stubs/ProductList.json";
  return request(url, "GET", {}, data, false)
    .then(response => {
      return response;
    })
    .catch(error => {
      throw error;
    });
}






const apiService = {
  getEvent,
  getData,
  postData,
  deleteData,
  patchData,
  logout,
  getProductList
  
};

export default apiService;
